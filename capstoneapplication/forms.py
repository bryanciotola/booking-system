from django import forms
from django.db.models import DateTimeField
from django.forms import DateInput
import datetime as dt
from django.forms import DateTimeInput


from capstoneapplication.models import Caterer, Customer, Venue, Booking


class CatererForm(forms.ModelForm):
    class Meta:
        model = Caterer
        fields = '__all__'

    def clean_caterer_name(self):
        return self.cleaned_data['caterer_name'].strip()

    def clean_caterer_phone(self):
        return self.cleaned_data['caterer_phone'].strip()

    def clean_caterer_email(self):
        return self.cleaned_data['caterer_email'].strip()

    def clean_caterer_menu(self):
        return self.cleaned_data['caterer_menu_type'].strip()

    def clean_caterer_menu_cost_per_person(self):
        return self.cleaned_data['caterer_menu_cost_per_person'].strip()


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = '__all__'


class VenueForm(forms.ModelForm):
    class Meta:
        model = Venue
        fields = '__all__'


class DateInput(forms.DateInput):
    input_type = 'date'


# class DateTimeInput(forms.DateTimeInput):
#     input_type ='text'
#
#     def __init__(self, **kwargs):
#         kwargs["format"] = "%H:%M"
#         super().__init__(**kwargs)


class BookingForm(forms.ModelForm):

    class Meta:
        model = Booking
        fields = '__all__'
        widgets = {
            'booking_date': DateInput(),
            'booking_start_time': forms.DateTimeInput(format='%m/%d/%y %H:%M'),
            'booking_end_time' : forms.DateTimeInput(format='%m/%d/%y %H:%M'),

        }

