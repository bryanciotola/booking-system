from multiprocessing import connection

from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.http.response import HttpResponse
from django.template import loader
from django.views import View
from django.views.generic import ListView, CreateView
from .filters import BookingFilter
from capstoneapplication.forms import CatererForm, CustomerForm, VenueForm, BookingForm
from capstoneapplication.utils import ObjectCreateMixin
from .models import (
    Caterer,
    Customer,
    Venue,
    Booking,
)

# FUNCTION BASED VIEW
# def caterer_list_view(request):
#     caterer_list = Caterer.objects.all()
#     # caterer_list = Caterer.objects.none()
#     template = loader.get_template(
#         'capstoneapplication/caterer_list.html')
#     context = {'caterer_list': caterer_list}
#     output = template.render(context)
#     return HttpResponse(output)


#CLASS BASED VIEW
class CatererList(ListView):
    model = Caterer


class CatererDetail(View):

    def get(self, request, pk):
        caterer = get_object_or_404(
            Caterer,
            pk=pk
        )
        booking_list = caterer.bookings.all()
        return render(
            request,
            'capstoneapplication/caterer_detail.html',
            {'caterer': caterer, 'booking_list': booking_list}
        )


class CatererCreate(CreateView):
    form_class = CatererForm
    model = Caterer


class CatererUpdate(View):
    form_class = CatererForm
    model = Caterer
    template_name = 'capstoneapplication/caterer_form_update.html'

    def get_object(self, pk):
        return get_object_or_404(
            self.model,
            pk=pk)

    def get(self, request, pk):
        caterer = self.get_object(pk)
        context = {
            'form': self.form_class(
                instance=caterer),
        'caterer': caterer,
        }
        return render(
            request, self.template_name, context)

    def post(self, request, pk):
        caterer = self.get_object(pk)
        bound_form = self.form_class(
            request.POST, instance=caterer)
        if bound_form.is_valid():
            new_caterer = bound_form.save()
            return redirect(new_caterer)
        else:
            context = {
                'form': bound_form,
                'caterer': caterer,
            }
            return render(
                request,
                self.template_name,
                context
        )


class CustomerList(ListView):
    model = Customer


class CustomerDetail(View):

    def get(self, request, pk):
        customer = get_object_or_404(
            Customer,
            pk=pk
        )
        booking_list = customer.bookings.all()
        return render(
            request,
            'capstoneapplication/customer_detail.html',
            {'customer': customer, 'booking_list': booking_list}
        )


class CustomerCreate(CreateView):
    form_class = CustomerForm
    model = Customer


class CustomerUpdate(View):
    form_class = CustomerForm
    model = Customer
    template_name = 'capstoneapplication/customer_form_update.html'

    def get_object(self, pk):
        return get_object_or_404(
            self.model,
            pk=pk)

    def get(self, request, pk):
        customer = self.get_object(pk)
        context = {
            'form': self.form_class(
                instance=customer),
        'customer': customer,
        }
        return render(
            request, self.template_name, context)

    def post(self, request, pk):
        customer = self.get_object(pk)
        bound_form = self.form_class(
            request.POST, instance=customer)
        if bound_form.is_valid():
            new_customer = bound_form.save()
            return redirect(new_customer)
        else:
            context = {
                'form': bound_form,
                'customer': customer,
            }
            return render(
                request,
                self.template_name,
                context
        )


class VenueList(ListView):
    model = Venue


class VenueDetail(View):

    def get(self, request, pk):
        venue = get_object_or_404(
            Venue,
            pk=pk
        )
        booking_list = venue.bookings.all()
        return render(
            request,
            'capstoneapplication/venue_detail.html',
            {'venue': venue, 'booking_list': booking_list}
        )


class VenueCreate(CreateView):
    form_class = VenueForm
    model = Venue


class VenueUpdate(View):
    form_class = VenueForm
    model = Venue
    template_name = 'capstoneapplication/venue_form_update.html'

    def get_object(self, pk):
        return get_object_or_404(
            self.model,
            pk=pk)

    def get(self, request, pk):
        venue = self.get_object(pk)
        context = {
            'form': self.form_class(
                instance=venue),
        'venue': venue,
        }
        return render(
            request, self.template_name, context)

    def post(self, request, pk):
        venue = self.get_object(pk)
        bound_form = self.form_class(
            request.POST, instance=venue)
        if bound_form.is_valid():
            new_venue = bound_form.save()
            return redirect(new_venue)
        else:
            context = {
                'form': bound_form,
                'venue': venue,
            }
            return render(
                request,
                self.template_name,
                context
        )


class BookingList(View):

    def get(self, request, *args, **kwargs):
        booking_list = Booking.objects.all()
        booking_by_customer = Booking.objects.all().prefetch_related("customer")
        return render(request, 'capstoneapplication/booking_list.html', {'booking_list': booking_list, 'booking_by_customer': booking_by_customer})


class ReportList(View):

    def get(self, request, *args, **kwargs):
        booking_by_customer = Booking.objects.all().prefetch_related("customer")
        return render(request, 'capstoneapplication/report_list.html', {'booking_by_customer': booking_by_customer})


class ReportList2(View):

        def get(self, request, *args, **kwargs):
            booking_by_venue = Booking.objects.all().prefetch_related("venue")
            return render(request, 'capstoneapplication/report_list2.html', {'booking_by_venue': booking_by_venue})


class BookingDetail(View):

    def get(self, request, pk):
        booking = get_object_or_404(
            Booking,
            pk=pk
        )
        venue = booking.venue
        customer = booking.customer
        caterer = booking.caterer
        return render(
            request,
            'capstoneapplication/booking_detail.html',
            {'booking': booking,
             'venue': venue,
             'customer': customer,
             'caterer': caterer}
        )


class BookingCreate(CreateView):
    form_class = BookingForm
    model = Booking


class BookingUpdate(View):
    form_class = BookingForm
    model = Booking
    template_name = 'capstoneapplication/booking_form_update.html'

    def get_object(self, pk):
        return get_object_or_404(
            self.model,
            pk=pk)

    def get(self, request, pk):
        booking = self.get_object(pk)
        context = {
            'form': self.form_class(
                instance=booking),
        'booking': booking,
        }
        return render(
            request, self.template_name, context)

    def post(self, request, pk):
        booking = self.get_object(pk)
        bound_form = self.form_class(
            request.POST, instance=booking)
        if bound_form.is_valid():
            new_booking = bound_form.save()
            return redirect(new_booking)
        else:
            context = {
                'form': bound_form,
                'booking': booking,
            }
            return render(
                request,
                self.template_name,
                context
        )


def search(request):
    booking_list = Booking.objects.all()
    booking_filter = BookingFilter(request.GET, queryset=booking_list)
    return render(request, 'search/booking_list.html', {'filter': booking_filter})

