from django.db import models
from django.forms import DateTimeInput
# Create your models here.
from django.conf import settings
from django.db import models
from django.utils import timezone
from datetime import datetime

from django.urls import reverse


class Caterer(models.Model):
    caterer_id = models.AutoField(primary_key = True)
    caterer_name = models.CharField(max_length = 100, default="")
    caterer_phone = models.CharField(max_length = 100, default="")
    caterer_email = models.CharField(max_length = 100, default="")
    caterer_menu_type = models.CharField(max_length = 100, default="")
    caterer_menu_cost_per_person = models.CharField(max_length = 100, default="")

    def __str__(self):
        return '%s, %s' % (self.caterer_name, self.caterer_menu_type)

    def get_absolute_url(self):
        return reverse('capstoneapplication_caterer_detail_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_update_url(self):
        return reverse('capstoneapplication_caterer_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )


class Customer(models.Model):
    customer_id = models.AutoField(primary_key = True)
    customer_first_name = models.CharField(max_length=100, default="")
    customer_last_name = models.CharField(max_length=100, default="")
    customer_street_address = models.CharField(max_length=100, default="")
    customer_city = models.CharField(max_length=100, default="")
    customer_state = models.CharField(max_length=100, default="")
    customer_zip = models.CharField(max_length=5, default="")
    customer_phone = models.CharField(max_length=100, default="")
    customer_email = models.CharField(max_length=100, default="")
    customer_organization = models.CharField(max_length=100, default="")

    def __str__(self):
        return self.customer_first_name + ' ' + self.customer_last_name + ', ' + self.customer_organization

    def get_absolute_url(self):
        return reverse('capstoneapplication_customer_detail_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_update_url(self):
        return reverse('capstoneapplication_customer_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['customer_organization','customer_first_name']


class Venue(models.Model):
    venue_id = models.AutoField(primary_key = True)
    venue_room_name = models.CharField(max_length=100, default="")
    venue_rental_fee = models.CharField(max_length=100, default="")
    venue_room_size = models.BigIntegerField()
    venue_room_capacity = models.BigIntegerField()
    # is_booked = models.BooleanField(default='True')

    def __str__(self):
        return self.venue_room_name

    def get_absolute_url(self):
        return reverse('capstoneapplication_venue_detail_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_update_url(self):
        return reverse('capstoneapplication_venue_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['venue_room_name','venue_id']


class Booking(models.Model):
    booking_id = models.AutoField(primary_key = True)
    caterer = models.ForeignKey(Caterer, related_name = 'bookings', on_delete = models.PROTECT)
    customer = models.ForeignKey(Customer, related_name = 'bookings', on_delete = models.PROTECT)
    venue = models.ForeignKey(Venue, related_name = 'bookings', on_delete = models.PROTECT)
    # booking_date = models.DateTimeField(auto_now_add=True)
    booking_date = models.DateTimeField(null=True, blank=True)
    booking_start_time = models.DateTimeField(null=True, blank=True)
    booking_end_time = models.DateTimeField(null=True, blank=True)
    is_cancelled = models.BooleanField()

    def __str__(self):
        # return str(self.customer)
        return str(self.venue) + ' - ' + str(self.booking_start_time)

    def get_absolute_url(self):
        return reverse('capstoneapplication_booking_detail_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_update_url(self):
        return reverse('capstoneapplication_booking_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        unique_together = ('venue', 'booking_start_time', 'booking_end_time')
