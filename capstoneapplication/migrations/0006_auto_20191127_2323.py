# Generated by Django 2.2.7 on 2019-11-28 05:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('capstoneapplication', '0005_auto_20191127_2315'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='customer',
            options={'ordering': ['customer_organization', 'customer_first_name']},
        ),
    ]
