from django.contrib.auth.models import User
import django_filters

from capstoneapplication import forms

from .models import Booking


class BookingFilter(django_filters.FilterSet):
    class Meta:
        model = Booking
        fields = ['venue', 'booking_date', 'booking_start_time', 'booking_end_time']
